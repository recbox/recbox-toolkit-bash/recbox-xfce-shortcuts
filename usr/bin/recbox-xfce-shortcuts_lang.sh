#!/usr/bin/env bash

SYS_LANG=$(echo "$LANG" | cut -d '.' -f 1)
if [[ -z $(find /usr/share/recbox-xfce-shortcuts/translations/ -name "$SYS_LANG".trans) ]]; then
    source /usr/share/recbox-xfce-shortcuts/translations/en_US.trans
else
    for TRANS in /usr/share/recbox-xfce-shortcuts/translations/"$SYS_LANG"*; do
        source $TRANS
    done
fi
